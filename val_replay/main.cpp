#include <mvd/ewap/ewap_settings.hpp>
#include <mvd/ewap/xp_wap_depth.hpp>

#include <infoapi.pb.h>

#include <arrow/io/file.h>
#include <parquet/stream_reader.h>
#include <parquet/stream_writer.h>
#include <parquet/types.h>

void DepthUpdate(auto &depth, auto &ewap, auto &current_orders) {
  const auto proto_side =
      depth.side == 0 ? ProtoCommon::Side::BUY : ProtoCommon::Side::SELL;

  if (depth.listing_id != 1000025313321338) {
    std::cout << "SOMETHING VERY WRONG" << std::endl;;
  }
  // Insert or amend

  if (depth.type == 3) {
    depth.type = current_orders.contains(depth.order_id) ? 1 : 0;
  }

  if (depth.type == 0) {
    current_orders.emplace(depth.order_id);
  }

  if (depth.type == 2 && !current_orders.erase(depth.order_id)) {
    return;
  }

  InfoApi::IncrementalOrderL2Update update;
  auto *order = update.add_order();
  order->set_instrument_id(depth.underlying_id);
  order->set_order_id(depth.order_id);
  order->set_action(static_cast<InfoApi::MDUpdateAction>(depth.type + 1));
  order->set_timestamp(depth.exch_ts /
                       1'000); // This gets multiplied back out :)))))
  order->set_side(proto_side);
  order->set_price(depth.price);
  order->set_volume(depth.volume);
  order->set_exchange("deri");

  ewap.OnOrderL2Update(update);
}

void TradeUpdate(auto &trade, auto &ewap) {
  if (trade.listing_id != 1000025313321338) {
    std::cout << "SOMETHING VERY WRONG" << std::endl;;
  }

  ewap.OnTradeTick(trade.price, trade.volume, trade.exch_ts, trade.exch_ts);
}

int main() {

  arrow::MemoryPool *pool = arrow::default_memory_pool();

  const auto path_to_file = "/home/claudio/dump/depth_d1_20240906.pq";
  const auto path_to_trades = "/home/claudio/dump/trades_d1_20240906.pq";
  const auto path_to_out_file = "./replay_vals.pq";

  // Input
  std::shared_ptr<arrow::io::ReadableFile> infile;
  PARQUET_ASSIGN_OR_THROW(infile, arrow::io::ReadableFile::Open(path_to_file));
  parquet::StreamReader depth_stream{parquet::ParquetFileReader::Open(infile)};

  std::shared_ptr<arrow::io::ReadableFile> trades_infile;
  PARQUET_ASSIGN_OR_THROW(trades_infile,
                          arrow::io::ReadableFile::Open(path_to_trades));
  parquet::StreamReader trades_stream{
      parquet::ParquetFileReader::Open(trades_infile)};

  struct {
    uint64_t sequence_id;
    uint64_t listing_id;
    uint64_t underlying_id;
    uint64_t order_id;
    uint64_t prev_order_id;
    double price;
    double volume;
    uint8_t side;
    uint8_t type;
    int64_t exch_ts;
    int64_t recv_ts;
    int64_t packet_ts;
    int64_t trace_ts;
    int32_t trace_host_id;
    int32_t trace_thread_id;
  } depth;

  struct {
    uint64_t sequence_id;
    uint64_t listing_id;
    uint64_t underlying_id;
    double price;
    double volume;
    uint8_t side;
    uint8_t type;
    uint64_t aggr_id;
    uint64_t pass_id;
    uint16_t pflag;
    uint8_t exch;
    int64_t exch_ts;
    int64_t recv_ts;
    int64_t packet_ts;
    int64_t trace_ts;
    int32_t trace_host_id;
    int32_t trace_thread_id;
  } trade;

  // ewap
  const auto settings =
      EWAP::xpWAPSettings(EWAP::min_delta_t{200000},         //
                          EWAP::bb_width_t{2},               //
                          EWAP::bb_volume_t{200000},         //
                          EWAP::lambda_t{0.6},               //
                          EWAP::iv_mult_t{0.5},              //
                          EWAP::iv_hl_t{0.5},                //
                          EWAP::pv_mult_t{0.5},              //
                          EWAP::pv_hl_t{0.5},                //
                          EWAP::tr_mult_t{2.0},              //
                          EWAP::tr_hl_t{1.0},                //
                          EWAP::option_mult_t{0.0},          //
                          std::chrono::microseconds(100000), //
                          std::chrono::microseconds(7500000));

  auto ewap = EWAP::EWAP_Depth{settings, 0, 1};
  std::set<uint64_t> current_orders;

  // output
  std::shared_ptr<arrow::io::FileOutputStream> outfile;
  PARQUET_ASSIGN_OR_THROW(outfile,
                          arrow::io::FileOutputStream::Open(path_to_out_file));

  parquet::schema::NodeVector fields;

  // clang-format off
  fields.push_back(parquet::schema::PrimitiveNode::Make("timestamp_ns", parquet::Repetition::OPTIONAL, parquet::Type::INT64, parquet::ConvertedType::INT_64));
  /*
  fields.push_back(parquet::schema::PrimitiveNode::Make("exch_timestamp_ns", parquet::Repetition::OPTIONAL, parquet::Type::INT64, parquet::ConvertedType::UINT_64));
  fields.push_back(parquet::schema::PrimitiveNode::Make("inst_id", parquet::Repetition::OPTIONAL, parquet::Type::INT64, parquet::ConvertedType::UINT_64));
  */
  fields.push_back(parquet::schema::PrimitiveNode::Make("bid_px", parquet::Repetition::OPTIONAL, parquet::Type::DOUBLE));
  fields.push_back(parquet::schema::PrimitiveNode::Make("ask_px", parquet::Repetition::OPTIONAL, parquet::Type::DOUBLE));
  fields.push_back(parquet::schema::PrimitiveNode::Make("bid_qty", parquet::Repetition::OPTIONAL, parquet::Type::INT32, parquet::ConvertedType::UINT_32));
  fields.push_back(parquet::schema::PrimitiveNode::Make("ask_qty", parquet::Repetition::OPTIONAL, parquet::Type::INT32, parquet::ConvertedType::UINT_32));
  fields.push_back(parquet::schema::PrimitiveNode::Make("valuation", parquet::Repetition::OPTIONAL, parquet::Type::DOUBLE));
  fields.push_back(parquet::schema::PrimitiveNode::Make("valid", parquet::Repetition::OPTIONAL, parquet::Type::BOOLEAN));
  fields.push_back(parquet::schema::PrimitiveNode::Make("trade_triggered", parquet::Repetition::OPTIONAL, parquet::Type::BOOLEAN));
  // clang-format on

  std::shared_ptr<parquet::schema::GroupNode> schema_ =
      std::static_pointer_cast<parquet::schema::GroupNode>(
          parquet::schema::GroupNode::Make(
              "schema", parquet::Repetition::OPTIONAL, fields));

  parquet::WriterProperties::Builder builder;

  parquet::StreamWriter os = parquet::StreamWriter{
      parquet::ParquetFileWriter::Open(outfile, schema_, builder.build())};

  // Loop
  int64_t next_exch_ts_log = 0;
  constexpr int64_t interval = 60l * 60 * 1'000 * 1'000 * 1'000;

  bool pulled_depth_last = false;

  const auto update_trade_stream = [&]() {
    while (trade.listing_id != 1000025313321338 && !trades_stream.eof()) {
      trades_stream >> trade.sequence_id >> trade.listing_id >>
          trade.underlying_id >> trade.price >> trade.volume >> trade.side >>
          trade.type >> trade.aggr_id >> trade.pass_id >> trade.pflag >>
          trade.exch >> trade.exch_ts >> trade.recv_ts >> trade.packet_ts >>
          trade.trace_ts >> trade.trace_host_id >> trade.trace_thread_id >>
          parquet::EndRow;
    }
    if (trades_stream.eof()) {
      trade.exch_ts = std::numeric_limits<int64_t>::max();
    }
  };

  const auto update_depth_stream = [&]() {
    while (depth.listing_id != 1000025313321338 && !depth_stream.eof()) {
      depth_stream >> depth.sequence_id >> depth.listing_id >>
          depth.underlying_id >> depth.order_id >> depth.prev_order_id >>
          depth.price >> depth.volume >> depth.side >> depth.type >>
          depth.exch_ts >> depth.recv_ts >> depth.packet_ts >> depth.trace_ts >>
          depth.trace_host_id >> depth.trace_thread_id >> parquet::EndRow;
    }
  };

  update_depth_stream();
  update_trade_stream();

  int64_t timestamp_ns = 0;

  while (!depth_stream.eof()) {

    bool was_trade;
    if (depth.exch_ts < trade.exch_ts) {
      DepthUpdate(depth, ewap, current_orders);
      timestamp_ns = depth.exch_ts;
      was_trade = false;
    } else {
      TradeUpdate(trade, ewap);
      timestamp_ns = trade.exch_ts;
      was_trade = true;
    }

    if (timestamp_ns > next_exch_ts_log) {
      std::cout << timestamp_ns << " was_trade: " << was_trade << std::endl;
      std::cout << "seq_id: " << depth.sequence_id << " ts: " << depth.exch_ts <<  " listing_id: " << depth.listing_id << std::endl;
      std::cout << "seq_id: " << trade.sequence_id << " ts: " << trade.exch_ts <<  " listing_id: " << trade.listing_id << std::endl;
      next_exch_ts_log = timestamp_ns + interval;
    }

    if(was_trade) {
      trade.listing_id = 0;
      update_trade_stream();
    } else {
      depth.listing_id = 0;
      update_depth_stream();
    }

    auto [val, _, _2] = ewap.EWAP_Unsafe(timestamp_ns);
    const bool valid = val > 0;


    // This is awful
    if (!valid) {
      val = 0;
    }

    const auto &book = ewap.Book();

    const auto [bid_px, bid_qty, _3] = book.TopBid();
    const auto [ask_px, ask_qty, _4] = book.TopAsk();

    // clang-format off
    os << timestamp_ns
       << (double)bid_px
       << (double)ask_px
       << bid_qty
       << ask_qty
       << val
       << valid
       << was_trade
       << parquet::EndRow;
   //clang-format on

  }
}
