from conans import ConanFile, CMake

class AtsConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = ( "mvdewap/3.5.4@mvd/stable","arrow/15.0.0", "boost/1.85.0", "mvdprotocol_suite/1.18.3@mvd/stable")

    build_requires = "gtest/cci.20210126"
    generators = "cmake", "cmake_find_package"

    def configure(self):
        self.options["arrow"].parquet = True
        self.options["arrow"].with_boost = True
        self.options["arrow"].with_thrift = True
        self.options["arrow"].with_zstd = True

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_EXPORT_COMPILE_COMMANDS"] = "YES"
        cmake.definitions["MVD_CONAN_BUILD"] = "On"
        cmake.configure()
        cmake.build()
