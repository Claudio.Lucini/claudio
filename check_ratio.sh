#!/bin/bash

for file in /home/clucini/dump/*; do
    echo $file
  if [[ "$file" == *"ll.log"  ]]; then
    NUM_TRADES=$(grep "OnPartial" $file -c)  
    NUM_AMEND=$(grep "OnOrderAmend" $file -c)
    NUM_LAST_TRADED_PULL=$(grep "autopull.*DEFAULT" $file -c)
    NUM_QUOTE_TIMEOUT=$(grep "quote.*TIME" $file -c)
    SUM_OF_ALL=$(expr $NUM_TRADES + $NUM_AMEND + $NUM_QUOTE_TIMEOUT + $NUM_LAST_TRADED_PULL)
    RED='\033[0;31m'
    NC='\033[0m' # No Color


    if [[ $NUM_TRADES -gt 0 ]]; then
        RATIO=$(awk "BEGIN {printf \"%.2f\",${SUM_OF_ALL}/${NUM_TRADES}}")
        LARGE_RATIO=$(awk -v num1=$RATIO 'BEGIN{ print (10<num1) ? "Y" : "N" }')
        if [ "$LARGE_RATIO" == "Y" ]; then
            printf "${RED}$file [trades: $NUM_TRADES, amends: $NUM_AMEND, quote_timeout: $NUM_QUOTE_TIMEOUT, last_traded_pull: $NUM_LAST_TRADED_PULL, ratio: $RATIO] Ratio > 10, please double check ${NC} \n"
        else 
            printf "$file [trades: $NUM_TRADES, amends: $NUM_AMEND, quote_timeout: $NUM_QUOTE_TIMEOUT, last_traded_pull: $NUM_LAST_TRADED_PULL, ratio: $RATIO ] \n"
        fi
    elif [[ $SUM_OF_ALL -gt 0 ]]; then
        printf "${RED}$file Has no trades but inserted ${SUM_OF_ALL} times please check ${NC} \n"
    fi
  fi
done
